import numpy as np
import pandas as pd
from pandas import DataFrame, Series
# from IPython.display import Image 
from io import StringIO
import pydotplus
from math import log, e
'exec(%matplotlib inline)'

df = pd.read_csv('kokvid.csv')
df['body temperature (Celcius)'] = df['body temperature (Celcius)'].gt(37.5)
df['body temperature (Celcius)'] = df['body temperature (Celcius)'].astype(int)
del df['Unnamed: 21']
del df['Unnamed: 22']
del df['Unnamed: 23']
df['age_lt_30'] = df['age'].lt(30)
df['age_bt_30_45'] = df['age'].between(30, 45, inclusive=True)
df['age_gt_45'] = df['age'].gt(45)
df['age_lt_30'] = df['age_lt_30'].astype(int)
df['age_bt_30_45'] = df['age_bt_30_45'].astype(int)
df['age_gt_45'] = df['age_gt_45'].astype(int)
del df['travel history to infected countries']
del df['Sno']
del df['age']

data_kecamatan = pd.read_csv('kecamatan.csv')

df = df[['age_lt_30', 'age_bt_30_45', 'age_gt_45', 'gender', 'body temperature (Celcius)', 'Dry Cough',
       'sour throat', 'weakness', 'breathing problem', 'drowsiness',
       'pain in chest', 'diabetes', 'heart disease', 'lung disease',
       'stroke or reduced immunity', 'symptoms progressed',
       'high blood pressue', 'kidney disease', 'change in appetide', 'Loss of sense of smell', 'Corona result']]

# df = df[['0', '1', '2','3', '4', '5', '6','7', '8', '9', '10','11', '12', '13', '14','15', '16','17', '18', '19', '20', '21']]

is_decision_tree_created = False
decision_tree = None

def entropy(df_label):
    classes,class_counts = np.unique(df_label,return_counts = True)
    entropy_value = np.sum([(-class_counts[i]/np.sum(class_counts))*np.log2(class_counts[i]/np.sum(class_counts)) 
                        for i in range(len(classes))])
    return entropy_value

def information_gain(dataset,feature,label): 
    # Calculate the dataset entropy
    dataset_entropy = entropy(dataset[label])   
    values,feat_counts= np.unique(dataset[feature],return_counts=True)
    
    # Calculate the weighted feature entropy                                
    # Call the calculate_entropy function
    weighted_feature_entropy = np.sum([(feat_counts[i]/np.sum(feat_counts))*entropy(dataset.where(dataset[feature]
                              ==values[i]).dropna()[label]) for i in range(len(values))])    
    feature_info_gain = dataset_entropy - weighted_feature_entropy
    return feature_info_gain

def create_decision_tree(dataset,df,features,label,parent):
  datum = np.unique(df[label], return_counts= True)

  unique_data = np.unique(dataset[label])
  if len(unique_data) <= 1:
      return unique_data[0]
  elif len(dataset) == 0:
      return unique_data[np.argmax(datum[1])]
  elif len(features) == 0:
      return parent
  else:
    parent = unique_data[np.argmax(datum[1])]

    item_values = [information_gain(dataset,feature,label) for feature in features]

    optimum_feature_index = np.argmax(item_values)
    optimum_feature = features[optimum_feature_index]
    decision_tree = {optimum_feature :{}}
    features = [i for i in features if i != optimum_feature]

    for value in np.unique(dataset[optimum_feature]):
        min_data = dataset.where(dataset[optimum_feature] == value).dropna()
        min_tree = create_decision_tree(min_data, df, features, label, parent)
        decision_tree[optimum_feature][value] = min_tree
    return(decision_tree)

def predict_risk(test_data, decision_tree):
    for nodes in decision_tree.keys():
        value = test_data[nodes]
        decision_tree = decision_tree[nodes][value]
        prediction = 0
        if type(decision_tree) is dict:
            prediction = predict_risk(test_data, decision_tree)
        else:
            prediction = decision_tree
            break

    return prediction

def predict_zone(kecamatan):
    row_kecamatan = data_kecamatan.loc[data_kecamatan['Nama Kecamatan'] == kecamatan]
    print(row_kecamatan)
    percentage = float(row_kecamatan.values[0][2])
    
    if percentage > 10:
        return "Red Zone"
    else:
        return "Low Risk Zone"

def get_rs(kecamatan):
    row_kecamatan = data_kecamatan.loc[data_kecamatan['Nama Kecamatan'] == kecamatan]
    kota = row_kecamatan.values[0][3]

    if kota=="Jakarta Selatan":
        return "RSUP Fatmawati"
    elif kota=="Jakarta Pusat":
        return "RSAL Dr. Mintoharjo, RSPAD Gatot Soebroto, RSUD Tarakan Jakarta"
    elif kota=="Jakarta Timur":
        return "RS Bhayangkara Tk. I R. Said Sukanto, RSUD Pasar Rebo, RSUP Persahabatan"
    elif kota=="Jakarta Utara":
        return "RSPI Prof. Dr. Sulianti Saroso"
    else:
        return "RSUD Kalideres, RS Pelni"

def print_decision_tree(dt, depth=0):
    if isinstance(dt, dict):
        for key in dt.keys():
            print((depth * "   ") + f"[{key}]")
            for value in dt[key].keys():
                print((depth * "    ") + f"  {value} ?")
                print_decision_tree(dt[key][value], depth+1)
    else:
        print((depth * "   ") + f"[{dt}]")

def predict_output(input_data):
    # global is_decision_tree_created
    # global decision_tree
    
    features = df.columns[:-1]
    label = 'Corona result'
    parent = None
    # if(not decision_tree):
    decision_tree = create_decision_tree(df,df.copy(),features,label,parent)
    # is_decision_tree_created = True
    # print_decision_tree(decision_tree)
    data = pd.Series(input_data)
    prediction = predict_risk(data,decision_tree)
    if prediction == 0:
        prediction = "Low Risk"
    elif prediction == 1:
        prediction = "Moderate Risk"
    elif prediction == 2:
        prediction = "High Risk"
    # else:
    #     prediction = "Salah"
    return prediction

# sample_data = {'age_lt_30':0, 'age_bt_30_45':1, 'age_gt_45':0,'gender':0,'body temperature (Celcius)':1, 'Dry Cough':0, 'sour throat':0, 'weakness':0, 'breathing problem':0, 'drowsiness':1,'pain in chest':1, 'diabetes':1, 'heart disease':0, 'lung disease':0, 'stroke or reduced immunity':0, 'symptoms progressed':1, 'high blood pressue':1,'kidney disease':0, 'change in appetide':1,'Loss of sense of smell':0}
# print(predict_output(sample_data))
# features = df.columns[:-1]
# label = 'Corona result'
# parent = None
# decision_tree = create_decision_tree(df,df,features,label,parent)
# sample_data = {'age_lt_30':0, 'age_bt_30_45':1, 'age_gt_45':0,'gender':0,'body temperature (Celcius)':1, 'Dry Cough':0, 'sour throat':0, 'weakness':0, 'breathing problem':0, 'drowsiness':1,'pain in chest':1, 'diabetes':1, 'heart disease':0, 'lung disease':0, 'stroke or reduced immunity':0, 'symptoms progressed':1, 'high blood pressue':1,'kidney disease':0, 'change in appetide':1,'Loss of sense of smell':0}
# test_data = pd.Series(sample_data)
# prediction = predict_risk(test_data, decision_tree)

# print(check(prediction))