// QUESTIONS

window.onload = function() {
  const questions = [
    {
      "question": "How old are you?",
      "answer1": "Under 30",
      "answer1Total": "1",
      "answer2": "Between 30 - 45",
      "answer2Total": "2",
      "answer3": "Over 45",
      "answer3Total": "3"
    },
    {
      "question": "What is your gender?",
      "answer1": "Male",
      "answer1Total": "1",
      "answer2": "Female",
      "answer2Total": "0",
    },
    {
      "question": "What is your current body temperature?",
      "answer1": "Over or  37.5 celcius",
      "answer1Total": "1",
      "answer2": "Under 37.5 celcius",
      "answer2Total": "0",
    },
    {
      "question": "Do you experience dry cough?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you experience sour throat?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you feel weak?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you have trouble breathing?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you experience drowsiness?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you feel experience pain in the chest?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you have diabetes?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you have a heart disease?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you have lung disease?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you have stroke or reduced immunity?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Does the symptoms progressed?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you have high blood pressure?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you have kidney disease?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Do you have change in appetite?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    {
      "question": "Have you lost sense of smell?",
      "answer1": "Yes",
      "answer1Total": "1",
      "answer2": "No",
      "answer2Total": "0"
    },
    ]

    var outputData = {};
    let currentQuestion = 0;
    const totalQuestions =questions.length;

    function generateQuestions (index) {
        const question = questions[index];
        const option1Total = questions[index].answer1Total;
        const option2Total = questions[index].answer2Total;
        const option3Total = questions[index].answer3Total;

        //Populate html elements 
        questionEl.innerHTML = `${index + 1}. ${question.question}`
        option1.setAttribute('data-total', `${option1Total}`);
        // option1.defaultSelected = true
        option2.setAttribute('data-total', `${option2Total}`);
        option3.setAttribute('data-total', `${option3Total}`);

        option1.innerHTML = `${question.answer1}`
        option2.innerHTML = `${question.answer2}`
        option3.innerHTML = `${question.answer3}`

        if(question["answer3"] == undefined){
          document.getElementById("idOption3").style.visibility = "hidden";
        }
      //Select each question by passing it a particular index
  }

  function loadNextQuestion () {
    const selectedOption = document.querySelector('input[type="radio"]:checked');
        //Check if there is a radio input checked
        if(!selectedOption) {
            alert('Please select your answer!');
            return;
        }

    selectedOption.checked = false;
    const answerScore = Number(selectedOption.nextElementSibling.getAttribute('data-total'));
    if(currentQuestion == 0){
      // alert('eh copot')
      if(answerScore == 1){
        outputData[0] = 1
        outputData[1] = 0
        outputData[2] = 0
      }
      else if(answerScore == 2){
        outputData[0] = 0
        outputData[1] = 1
        outputData[2] = 0
      }
      else{
        outputData[0] = 0
        outputData[1] = 0
        outputData[2] = 1
      }
    }
    // else if(currentQuestion == totalQuestions - 1){
    //   nextButton.textContent = 'Finish';
    //   // outputData[currentQuestion+2] = answerScore
      
    // }
    else {
      outputData[currentQuestion+2] = answerScore
    }
    
    currentQuestion++;
    if(currentQuestion == (totalQuestions - 1)) {
      nextButton.textContent = 'Finish';
      outputData[currentQuestion+2] = answerScore
    }
    
    generateQuestions(currentQuestion);
    console.log(outputData)
  }
  


  const container = document.querySelector('.quiz-container');
  const questionEl = document.querySelector('.question');
  const option1 = document.querySelector('.option1');
  const option2 = document.querySelector('.option2');
  const option3 = document.querySelector('.option3');
  const nextButton = document.querySelector('.next');
  const result = document.querySelector('.result');


  generateQuestions(currentQuestion);
  nextButton.addEventListener('click', function(){
    if(currentQuestion == (totalQuestions-1)){
      const selectedOption = document.querySelector('input[type="radio"]:checked');
      const answerScore = Number(selectedOption.nextElementSibling.getAttribute('data-total'));
      outputData[currentQuestion+2] = answerScore;

      $('.loader').toggleClass("not-shown");
      $('.loadtext').toggleClass("not-shown");
      $(".main-login").css({ display: "none" });

      // Convert Output Data to JSON

      console.log(currentQuestion)
      console.log('atas')
      console.log(outputData)
      console.log('bawah')
      // $.('#result').html("Loading...")
      $.ajax({
        url: 'result/',
        type: 'post',
        // dataType: 'application/json',
        contentType: 'json',
        csrfmiddlewaretoken: x,
        data: JSON.stringify(outputData),
        success: function (data) {
          $('.loader').css({ display: "none" });
          $('.loadtext').css({ display: "none" });
          $(".main-login").css({ display: "block" });
          //Show HTML
          console.log(data)
          // $('#loading').hide();
          $('.main-login').html(data);
          //Show HTML code
          // $('#main-login').text(data);
          // console.log(data)

        },
        error: function (request, error) {
          console.log(arguments);
          alert(" Can't do because: " + error);
        },
      });
      //location.href = "result/answerresult/"

    }
    else{
      loadNextQuestion(); 
    }
  })

  // result.addEventListener('click',restartQuiz);
}
// $.post( "/postmethod", {
//   sample_data: JSON.stringify(outputData)
// }, function(err, req, resp){
//   window.location.href = "/results/"+resp["responseJSON"]["uuid"];  
// });


