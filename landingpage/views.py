from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import render_to_string
from django.http import JsonResponse, HttpResponse
from .kokvid import predict_output, predict_zone, get_rs
# import kokvid
import json
# from landingpage.kokvid import get_rs, predict_zone
# import pandas as pd
# Create your views here.

def index(request):
    return render(request, 'index.html')

def finish(request):
    return render(request, 'finish.html')

def test(request):
    return render(request, 'test.html')

def questions(request):
    return render(request, 'questions.html')

def kecamatan(request):
    return render(request, 'kecamatan.html')

def zone(request):
    return render(request, 'zone.html')

def rs(request):
    return render(request, 'hospital.html')

def rs_res(request,rs):
    editedRs = ""
    for alp in rs:
        if alp == "-":
            editedRs=editedRs+" "
        else:
            editedRs=editedRs+alp

    res = get_rs(editedRs)
    return render(request, "rs_res.html", {'res':res})

def nama_kec(request, kec):
    editedKec = ""
    for alp in kec:
        if alp == "-":
            editedKec=editedKec+" "
        else:
            editedKec=editedKec+alp

    zone = predict_zone(editedKec)
    return render(request, "zone.html", {'zone':zone})

@csrf_exempt
def result(request):
    if request.method == 'POST':
        response_json = request.body
        data = json.loads(response_json)
        # print(data)
        # print(predict_output(data))
        column = ['age_lt_30', 'age_bt_30_45', 'age_gt_45', 'gender', 'body temperature (Celcius)', 'Dry Cough',
       'sour throat', 'weakness', 'breathing problem', 'drowsiness',
       'pain in chest', 'diabetes', 'heart disease', 'lung disease',
       'stroke or reduced immunity', 'symptoms progressed',
       'high blood pressue', 'kidney disease', 'change in appetide', 'Loss of sense of smell']

        dic_data = {}
        for k,v in data.items():
           dic_data[column[int(k)]] = v

        # print(dic_data)
        output = predict_output(dic_data)
        # Jadiin JSON
        data = {'output': output}
        # print(output)
        # print(type(output))
        # print(type(data))
        html = render_to_string('output.html', data)
        return HttpResponse(html)
        # return render(request, 'output.html', data)
        # return JsonResponse(json.dumps(data))
    return render(request, 'output.html',{'output': "Masuk Else"})
