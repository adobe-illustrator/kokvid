from django.urls import path

from .views import index, questions, result, kecamatan, nama_kec, zone, rs, rs_res, finish, test

urlpatterns = [
    path('', index, name='index'),
    path('questions/', questions, name ='questions'),
    path('questions/kecamatan/', kecamatan, name ='kecamatan'),
    path('questions/kecamatan/rs/', rs, name ='rs'),
    path('questions/kecamatan/rs/<str:rs>', rs_res, name ='rs_res'),
    path('questions/result/', result, name='result'),
    path('questions/zone/', zone, name='zone'),
    path('questions/kecamatan/<str:kec>', nama_kec, name='nama_kec'),
    path('finish/', finish, name ='finish'),
    path('test/', test, name ='test'),
]
